open Mpeg3

let main() =
  let m = open_mpeg "/cdwriter-data/video/p06_0.mpeg" in
  set_mmx true m;
  let width = video_width 0 m in
  let height = video_height 0 m in
  let fb = create_frame_buffer ~width ~height in
  read_frame 0 fb m;

  let screen = Sdlvideo.set_video_mode ~w:width ~h:height ~bpp:24 
		 [ `HWSURFACE ] in
   
  let fb_sfc = Sdlvideo.create_RGB_surface_from_24 (fb_array1 fb) 
		 ~w:width ~h:height ~pitch:(width*3)
		 ~rmask:0x0000ff ~gmask:0x00ff00 ~bmask:0xff0000 ~amask:0
  in
  
  for k = 1 to 50 do
    Sdlvideo.blit_surface ~src:fb_sfc ~dst:screen ();
    Sdlvideo.flip screen;
    prerr_endline "next frame";
    read_frame 0 fb m;
  done;

  let _ = read_line() in
  ()
;;


Sdl.init [ `TIMER ; `VIDEO ];;

let _ =
  try main ()
  with exn -> Sdl.quit () ; raise exn
;;
