open GMain
open Mpeg3

let m = open_mpeg "/cdwriter-data/video/p06_0.mpeg";;
set_mmx true m;;
let width = video_width 0 m;;
let height = video_height 0 m;;
let fb = create_frame_buffer ~width ~height;;
read_frame 0 fb m;;

let region = Gpointer.region_of_bigarray (fb_array1 fb);;

let window = GWindow.window ~width ~height ()
let area = GMisc.drawing_area ~packing:window#add ()
 
let w = area#misc#realize (); area#misc#window
(* let drawing = new GDraw.drawable w *)
let gc = Gdk.GC.create w

let redraw _ =
  Gdk.Rgb.draw_image w gc ~width ~height region;
(*
  drawing#polygon ~filled:true
    [ 10,100; 35,35; 100,10; 165,35; 190,100;
      165,165; 100,190; 35,165; 10,100 ];
*)
  false
 
let rec next_frame() =
  read_frame 0 fb m;
  redraw();
  next_frame()
  (* false*)
;;


let _ =
  window#connect#destroy ~callback:Main.quit;
  area#event#connect#expose ~callback:redraw;
  window#show ();
  Timeout.add ~ms:40 (* 25 fps *) ~callback:next_frame;
  Main.main ()
