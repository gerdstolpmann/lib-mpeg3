(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Bigarray

type mpeg

type color_model =
    RGB565 
  | BGR888 
  | BGRA8888 
  | RGB888 
  | RGBA8888 
  | RGBA161616
  | RGB565_601
  | BGR888_601
  | BGRA8888_601
  | RGB888_601
  | RGBA8888_601
  | YUV420P
  | YUV422P

exception Error
let _ = Callback.register_exception "mpeg3_error" Error;;


external check_sig : string -> bool = "mpeg3ml_check_sig"
external open_mpeg : string -> mpeg = "mpeg3ml_open_mpeg"
external open_copy : string -> mpeg -> mpeg = "mpeg3ml_open_copy";;
external close : mpeg -> unit = "mpeg3ml_close"

external set_cpus : int -> mpeg -> unit = "mpeg3ml_set_cpus"
external set_mmx : bool -> mpeg -> unit = "mpeg3ml_set_mmx"

(*
external total_programs : mpeg -> int = "mpeg3ml_total_programs"
external set_program : int -> mpeg -> unit = "mpeg3ml_set_program"
*)

type int_prop = 
    A_exists | A_streams | A_channels | A_rate | A_end
  | V_exists | V_streams | V_width | V_height | V_end | V_cmodel
  

external get_int_property : int_prop -> int -> mpeg -> int 
  = "mpeg3ml_int_prop"


type int_sprop =
    V_previous | V_rowspan | V_drop

external set_int_property : int_sprop -> int -> int -> mpeg -> unit 
  = "mpeg3ml_int_sprop"


type nativeint_prop = A_length | A_pos | V_length | V_pos

external get_nativeint_property : nativeint_prop -> int -> mpeg -> nativeint
  = "mpeg3ml_nativeint_prop"

type nativeint_sprop = A_seek | V_seek

external set_nativeint_property : nativeint_sprop -> int -> nativeint -> mpeg -> unit
      = "mpeg3ml_nativeint_sprop"
      

type string_prop = A_format

external get_string_property : string_prop -> int -> mpeg -> string
      = "mpeg3ml_string_prop"


type float_prop = V_aspect | V_rate | Percentage | Time

external get_float_property : float_prop -> int -> mpeg -> float
      = "mpeg3ml_float_prop";;

type float_sprop = Seekp

external set_float_property : float_sprop -> int -> float -> mpeg -> unit
  = "mpeg3ml_float_sprop";;




let has_audio m =
  get_int_property A_exists 0 m = 1
;;

let audio_streams m =
  get_int_property A_streams 0 m
;;

let audio_channels ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.audio_channels";
  get_int_property A_channels stream m
;;

let audio_sample_rate ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.audio_sample_rate";
  get_int_property A_rate stream m
;;

let end_of_audio ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.end_of_audio";
  get_int_property A_end stream m = 1
;;


let audio_length ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.audio_length";
  get_nativeint_property A_length stream m
;;

let tell_audio_position ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.tell_audio_position";
  get_nativeint_property A_pos stream m
;;

let seek_audio_position ~stream ~pos m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.seek_audio_position";
  set_nativeint_property A_seek stream pos m
;;

let audio_format ~stream m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.audio_format";
  get_string_property A_format stream m
;;

let has_video m =
  get_int_property V_exists 0 m = 1
;;

let video_streams m =
  get_int_property V_streams 0 m 
;;

let video_width ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.video_width";
  get_int_property V_width stream m
;;

let video_height ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.video_height";
  get_int_property V_height stream m
;;

let end_of_video ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.end_of_video";
  get_int_property V_end stream m = 1
;;

let cm_array =
  [| RGB565;  BGR888; BGRA8888; RGB888; RGBA8888; RGBA161616;
     RGB565_601; BGR888_601; BGRA8888_601; RGB888_601; RGBA8888_601;
     YUV420P; YUV422P |];;


let color_model ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.color_model";
  let k = get_int_property V_cmodel stream m in
  cm_array.(k)
;;

let video_length ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.video_length";
  get_nativeint_property V_length stream m
;;

let tell_video_position ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.tell_video_position";
  get_nativeint_property V_pos stream m
;;

let seek_video_position ~stream ~pos m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.seek_video_position";
  set_nativeint_property V_seek stream pos m
;;

let seek_previous_frame ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.seek_previous_frame";
  set_int_property V_previous stream 0 m
;;

let drop_frames ~stream n m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.drop_frames";
  set_int_property V_drop stream n m
;;

let video_aspect_ratio ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.video_aspect_ratio";
  get_float_property V_aspect stream m
;;

let video_frame_rate ~stream m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.video_frame_rate";
  get_float_property V_rate stream m
;;

let seek_percentage p m =
  if p < 0.0 || p > 1.0 then 
    invalid_arg "Mpeg3.seek_percentage";
  set_float_property Seekp 0 p m
;;

let tell_percentage m =
  get_float_property Percentage 0 m
;;

let get_time m =
  get_float_property Time 0 m
;;

external read_audio : int -> int -> int -> int -> bool -> 
                      ('a,'b,c_layout) Bigarray.Array1.t -> mpeg -> unit
			= "mpeg3ml_read_audio_bc" "mpeg3ml_read_audio";;

let read_audio_int16 ~stream ~channel ?(pos=0) ?length ?(reread=false)
                     (buf:(int,int16_signed_elt,c_layout) Bigarray.Array1.t)
                     m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.read_audio_int16";

  if channel < 0 || channel >= audio_channels stream m then
    invalid_arg "Mpeg3.read_audio_int16";

  let size = Bigarray.Array1.dim buf in

  let len = 
    match length with
	None -> size - pos 
      | Some l -> l in

  if pos < 0 || pos > size || len < 0 || pos + len > size then
    invalid_arg "read_audio_int16";

  read_audio stream channel pos len reread buf m
;;


let read_audio_float ~stream ~channel ?(pos=0) ?length ?(reread=false)
                     (buf:(float,float32_elt,c_layout) Bigarray.Array1.t)
                     m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.read_audio_float";

  if channel < 0 || channel >= audio_channels stream m then
    invalid_arg "Mpeg3.read_audio_float";

  let size = Bigarray.Array1.dim buf in

  let len = 
    match length with
	None -> size - pos 
      | Some l -> l in

  if pos < 0 || pos > size || len < 0 || pos + len > size then
    invalid_arg "read_audio_float";

  read_audio stream channel pos len reread buf m
;;



external read_audio_chunk_x : int -> int -> int -> string -> mpeg -> int
  = "mpeg3ml_read_audio_chunk";;

let read_audio_chunk ~stream ?(pos=0) ?length s m =
  if stream < 0 || stream >= audio_streams m then
    invalid_arg "Mpeg3.read_audio_chunk";

  let size = String.length s in

  let len = 
    match length with
	None -> size - pos 
      | Some l -> l in

  if pos < 0 || pos > size || len < 0 || pos + len > size then
    invalid_arg "read_audio_chunk";

  read_audio_chunk_x stream pos len s m
;;



external read_video_chunk_x : int -> int -> int -> string -> mpeg -> int
  = "mpeg3ml_read_video_chunk";;

let read_video_chunk ~stream ?(pos=0) ?length s m =
  if stream < 0 || stream >= video_streams m then
    invalid_arg "Mpeg3.read_video_chunk";

  let size = String.length s in

  let len = 
    match length with
	None -> size - pos 
      | Some l -> l in

  if pos < 0 || pos > size || len < 0 || pos + len > size then
    invalid_arg "read_video_chunk";

  read_video_chunk_x stream pos len s m
;;


type frame_buffer = ( (int,int8_unsigned_elt,c_layout) Array1.t *
		      (nativeint,nativeint_elt,c_layout) Array1.t *
		      int *
		      int )

     (* (cont_buf, row_buf, width, height)
      * cont_buf: The continous buffer with width*height+4 bytes
      * row_buf:  For every row of the picture this buffer contains the
      *           start address in cont_buf
      *)

external init_frame_buffer : (int,int8_unsigned_elt,c_layout) Array1.t ->
                             (nativeint,nativeint_elt,c_layout) Array1.t ->
			     int -> int ->
                             unit = "mpeg3ml_init_frame_buffer";;


let create_frame_buffer ~width ~height =
  if width <= 0 || height <= 0 then
    invalid_arg "Mpeg3.create_frame_buffer";

  let n = width * height * 3 + 4 in
  let cont_buf = Array1.create int8_unsigned c_layout n in
  let row_buf  = Array1.create nativeint c_layout height in
  init_frame_buffer cont_buf row_buf width height;
  (cont_buf, row_buf, width, height)
;;


let fb_width (_,_,w,h) = w;;
let fb_height (_,_,w,h) = h;;

let fb_array1 (cont_buf,_,w,h) =
  (* Discard the extra 4 bytes at the end of the last row *)
  let g = genarray_of_array1 cont_buf in
  let g' = Genarray.sub_left g 0 (w*h*3) in
  array1_of_genarray g'
;;


let fb_array2 (cont_buf,_,w,h) =
  (* Discard the extra 4 bytes at the end of the last row *)
  let g = genarray_of_array1 cont_buf in
  let g' = Genarray.sub_left g 0 (w*h*3) in
  (* Reshape as matrix *)
  reshape_2 g' h (w*3)
;;


external read_frame_x : (int*int*int*int) -> frame_buffer -> int -> mpeg -> unit
  = "mpeg3ml_read_frame";;

let read_frame ?(in_x=0) ?(in_y=0) ?in_w ?in_h ~stream fb m =
  let h = video_height ~stream m in  (* checks stream *)
  let w = video_width ~stream m in
  
  let in_w =
    match in_w with
	None -> w - in_x
      | Some v -> v in

  let in_h =
    match in_h with
	None -> h - in_y
      | Some v -> v in

  if in_x < 0 || in_x >= w || in_w <= 0 || in_x+in_w > w then
    invalid_arg "Mpeg3.read_frame";

  if in_y < 0 || in_y >= h || in_h <= 0 || in_y+in_h > h then
    invalid_arg "Mpeg3.read_frame";

  read_frame_x (in_x,in_y,in_w,in_h) fb stream m
;;
  

