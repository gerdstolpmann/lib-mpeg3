/* $Id$
 * ----------------------------------------------------------------------
 *
 */

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/custom.h>
#include <caml/intext.h>
#include <caml/bigarray.h>

#include <libmpeg3.h>

#define Mpeg3_val(v) ((mpeg3_t *) Field(v,0))

static void raise_error (void) {
    raise_constant(*caml_named_value("mpeg3_error"));
}



CAMLprim value mpeg3ml_check_sig(value path) {
    int r;

    /* no alloc possible */
    r = mpeg3_check_sig(String_val(path));
    return (r != 0 ? Val_true : Val_false);
}


CAMLprim value mpeg3ml_open_mpeg(value path) {
    mpeg3_t *m;
    value    mv;

    m = mpeg3_open(String_val(path));

    if (m == NULL) {
	raise_error();
    }
    
    /* path: do not use any longer! */

    mv = alloc(1, Abstract_tag);
    Field(mv, 0) = (value) m;
    
    return mv;
}


CAMLprim value mpeg3ml_open_copy(value path, value old_mv) {
    mpeg3_t *m, *old_m;
    value    mv;

    old_m = Mpeg3_val(old_mv);
    if (old_m == NULL) raise_error();

    m = mpeg3_open_copy(String_val(path), old_m);

    if (m == NULL) {
	raise_error();
    }
    
    /* path: do not use any longer! */

    mv = alloc(1, Abstract_tag);
    Field(mv, 0) = (value) m;
    
    return mv;
}


CAMLprim value mpeg3ml_close(value mv) {
    mpeg3_t *m;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m != NULL) {
	rc = mpeg3_close(m);
	if (rc < 0) raise_error();
    };
    /* m == NULL: descriptor is already closed */

    Mpeg3_val(mv) = NULL;

    return Val_unit;
}


CAMLprim value mpeg3ml_set_cpus (value cpus, value mv) {
    mpeg3_t *m;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    rc = mpeg3_set_cpus(m, Int_val(cpus));
    if (rc < 0) raise_error();

    return Val_unit;
}


CAMLprim value mpeg3ml_set_mmx (value flag, value mv) {
    mpeg3_t *m;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    rc = mpeg3_set_mmx(m, Bool_val(flag));
    if (rc < 0) raise_error();

    return Val_unit;
}


static int cm_array[] = { 
    MPEG3_RGB565,
    MPEG3_BGR888,
    MPEG3_BGRA8888,
    MPEG3_RGB888,
    MPEG3_RGBA8888,
    MPEG3_RGBA16161616,
    MPEG3_601_RGB565,
    MPEG3_601_BGR888,
    MPEG3_601_BGRA8888,
    MPEG3_601_RGB888,
    MPEG3_601_RGBA8888,
    MPEG3_YUV420P,
    MPEG3_YUV422P,
    -1
};


CAMLprim value mpeg3ml_int_prop (value propv, value sv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);

    prop = Int_val(propv);
    rc = -1;

    switch (prop) {
    case 0:
	rc = mpeg3_has_audio(m);
	rc = (rc != 0 ? 1 : 0);
	break;
    case 1:
	rc = mpeg3_total_astreams(m);
	break;
    case 2:
	rc = mpeg3_audio_channels(m, s);
	break;
    case 3:
	rc = mpeg3_sample_rate(m, s);
	break;
    case 4:
	rc = mpeg3_end_of_audio(m, s);
	rc = (rc != 0 ? 1 : 0);
	break;
    case 5:
	rc = mpeg3_has_video(m);
	rc = (rc != 0 ? 1 : 0);
	break;
    case 6:
	rc = mpeg3_total_vstreams(m);
	break;
    case 7:
	rc = mpeg3_video_width(m, s);
	break;
    case 8:
	rc = mpeg3_video_height(m, s);
	break;
    case 9:
	rc = mpeg3_end_of_video(m, s);
	rc = (rc != 0 ? 1 : 0);
	break;
    case 10: 
	{
	    int cm,k;
	    cm = mpeg3_colormodel(m, s);
	    k = 0;
	    while (cm_array[k] != (-1) && cm_array[k] != cm) k++;
	    if (cm_array[k] == cm) rc = k;
	    break;
	}
    }

    if (rc < 0) raise_error();

    return Val_int(rc);
}


CAMLprim value mpeg3ml_int_sprop (value propv, value sv, value xv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    int x;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    x = Int_val(xv);

    prop = Int_val(propv);
    rc = -1;

    switch (prop) {
    case 0:
	rc = mpeg3_previous_frame(m, s);
	break;
    case 1:
	rc = mpeg3_set_rowspan(m, x, s);
	break;
    case 2:
	rc = mpeg3_drop_frames(m, x, s);
	break;
    }

    if (rc != 0) raise_error();

    return Val_unit;
}


CAMLprim value mpeg3ml_nativeint_prop (value propv, value sv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    long rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);

    prop = Int_val(propv);
    rc = -1;

    switch (prop) {
    case 0:
	rc = mpeg3_audio_samples(m, s);
	break;
    case 1:
	rc = mpeg3_get_sample(m,s);
	break;
    case 2:
	rc = mpeg3_video_frames(m,s);
	break;
    case 3:
	rc = mpeg3_get_frame(m, s);
	break;
    }

    if (rc < 0) raise_error();

    return copy_nativeint(rc);
}


CAMLprim value mpeg3ml_nativeint_sprop (value propv, value sv, value xv, 
					value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    long x;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    x = Nativeint_val(xv);

    prop = Int_val(propv);
    rc = -1;

    switch (prop) {
    case 0:
	rc = mpeg3_set_sample(m, x, s);
	break;
    case 1:
	rc = mpeg3_set_frame(m, x, s);
	break;
    }

    if (rc != 0) raise_error();

    return Val_unit;
}


CAMLprim value mpeg3ml_string_prop (value propv, value sv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    char *rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);

    prop = Int_val(propv);
    rc = NULL;

    switch (prop) {
    case 0:
	rc = mpeg3_audio_format(m, s);
	break;
    }

    if (rc == NULL) raise_error();

    return copy_string(rc);
}


CAMLprim value mpeg3ml_float_prop (value propv, value sv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    double r;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);

    prop = Int_val(propv);

    switch (prop) {
    case 0:
	r = mpeg3_aspect_ratio(m, s);
	break;
    case 1:
	r = mpeg3_frame_rate(m, s);
	break;
    case 2:
	r = mpeg3_tell_percentage(m);
	break;
    case 3:
	r = mpeg3_get_time(m);
	break;
    }

    return copy_double(r);
}


CAMLprim value mpeg3ml_float_sprop (value propv, value sv, value xv, value mv) {
    mpeg3_t *m;
    int prop;
    int s;
    double x;
    int rc;
    
    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    x = Double_val(xv);

    prop = Int_val(propv);
    rc = -1;

    switch (prop) {
    case 0:
	rc = mpeg3_seek_percentage(m, x);
	break;
    }

    if (rc != 0) raise_error();
    

    return Val_unit;
}


CAMLprim value mpeg3ml_read_audio (value sv, /* stream */
				   value cv, /* channel */
				   value posv, 
				   value lenv,
				   value rereadv,
				   value bufferv, /* a bigarray */
				   value mv) {
    mpeg3_t *m;
    int s, c, pos, len, reread, buffermask, rc;
    float *output_f;
    short *output_i;

    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    c = Int_val(cv);
    pos = Int_val(posv);
    len = Int_val(lenv);
    reread = Bool_val(rereadv);

    output_f = NULL;
    output_i = NULL;

    buffermask = Bigarray_val(bufferv)->flags & BIGARRAY_KIND_MASK;
    if (buffermask == BIGARRAY_FLOAT32) {
	output_f = Data_bigarray_val(bufferv);
	output_f += pos;
    } else if (buffermask == BIGARRAY_SINT16) {
	output_i = Data_bigarray_val(bufferv);
	output_i += pos;
    } else raise_error();

    if (reread) {
	rc = mpeg3_reread_audio(m, output_f, output_i, c, len, s);
    } else {
	rc = mpeg3_read_audio(m, output_f, output_i, c, len, s);
    }

    if (rc != 0) raise_error();

    return Val_unit;
}


CAMLprim value mpeg3ml_read_audio_bc(value * argv, int argn)
{
  return mpeg3ml_read_audio(argv[0], argv[1], argv[2], argv[3],
			    argv[4], argv[5], argv[6]);
}


CAMLprim value mpeg3ml_read_audio_chunk (value sv, /* stream */
					 value posv, 
					 value lenv,
					 value bufferv, /* a string */
					 value mv) {
    mpeg3_t *m;
    int s, pos, len, rc;
    unsigned char *output;
    long size;

    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    pos = Int_val(posv);
    len = Int_val(lenv);
    output = String_val(bufferv);
    output += pos;

    rc = mpeg3_read_audio_chunk(m, output, &size, len, s);

    if (rc != 0) raise_error();

    return Val_long(size);
}


CAMLprim value mpeg3ml_read_video_chunk (value sv, /* stream */
					 value posv, 
					 value lenv,
					 value bufferv, /* a string */
					 value mv) {
    mpeg3_t *m;
    int s, pos, len, rc;
    unsigned char *output;
    long size;

    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    pos = Int_val(posv);
    len = Int_val(lenv);
    output = String_val(bufferv);
    output += pos;

    rc = mpeg3_read_video_chunk(m, output, &size, len, s);

    if (rc != 0) raise_error();

    return Val_long(size);
}


CAMLprim value mpeg3ml_init_frame_buffer(value cont_buf, value row_buf,
					 value widthv,   value heightv) {
    int width, height, row;
    unsigned char *cont_buf_c;
    unsigned char **row_buf_c;

    width = Int_val(widthv);
    height = Int_val(heightv);

    cont_buf_c = Data_bigarray_val(cont_buf);
    row_buf_c  = Data_bigarray_val(row_buf);

    for (row = 0; row < height; row++) {
	row_buf_c[row] = cont_buf_c + (row * width * 3);
    }

    return Val_unit;
}


CAMLprim value mpeg3ml_read_frame(value window, value fb, value sv, value mv) {
    mpeg3_t *m;
    int s, in_x, in_y, in_w, in_h, out_w, out_h, rc;
    unsigned char **row_buf_c;

    m = Mpeg3_val(mv);
    if (m == NULL) raise_error();

    s = Int_val(sv);
    in_x = Int_val(Field(window,0));
    in_y = Int_val(Field(window,1));
    in_w = Int_val(Field(window,2));
    in_h = Int_val(Field(window,3));

    out_w = Int_val(Field(fb,2));
    out_h = Int_val(Field(fb,3));
    row_buf_c = Data_bigarray_val(Field(fb,1));
   
    rc = mpeg3_read_frame(m, row_buf_c, in_x, in_y, in_w, in_h,
			  out_w, out_h, MPEG3_RGB888, s);

    if (rc != 0) raise_error();
    return Val_unit;
}
