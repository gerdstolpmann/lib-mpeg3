(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Bigarray

type mpeg

type color_model =
    RGB565 
  | BGR888 
  | BGRA8888 
  | RGB888 
  | RGBA8888 
  | RGBA161616
  | RGB565_601
  | BGR888_601
  | BGRA8888_601
  | RGB888_601
  | RGBA8888_601
  | YUV420P
  | YUV422P

type frame_buffer


exception Error


val check_sig : string -> bool

val open_mpeg : string -> mpeg
val open_copy : string -> mpeg -> mpeg
val close : mpeg -> unit

val set_cpus : int -> mpeg -> unit
val set_mmx : bool -> mpeg -> unit

(*
val total_programs : mpeg -> int
val set_program : int -> mpeg -> unit
*)

(**********************************************************************)
(* Audio                                                              *)
(**********************************************************************)

val has_audio : mpeg -> bool
val audio_streams : mpeg -> int
val audio_channels : stream:int -> mpeg -> int
val audio_sample_rate : stream:int -> mpeg -> int
val audio_format : stream:int -> mpeg -> string
val audio_length : stream:int -> mpeg -> nativeint

val seek_audio_position : stream:int -> pos:nativeint -> mpeg -> unit
val tell_audio_position : stream:int -> mpeg -> nativeint

val end_of_audio : stream:int -> mpeg -> bool

val read_audio_int16 : stream:int ->
                       channel:int ->
                       ?pos:int ->
                       ?length:int ->
                       ?reread:bool ->
                       (int, int16_signed_elt, c_layout) Bigarray.Array1.t ->
                       mpeg -> 
                       unit

val read_audio_float : stream:int ->
                       channel:int ->
                       ?pos:int ->
                       ?length:int ->
                       ?reread:bool ->
                       (float, float32_elt, c_layout) Bigarray.Array1.t ->
                       mpeg -> 
                       unit

val read_audio_chunk : stream:int ->
                       ?pos:int ->
                       ?length:int ->
                       string ->
                       mpeg ->
                       int

(**********************************************************************)
(* Video                                                              *)
(**********************************************************************)

val has_video : mpeg -> bool
val video_streams : mpeg -> int
val video_width : stream:int -> mpeg -> int
val video_height : stream:int -> mpeg -> int
val video_aspect_ratio : stream:int -> mpeg -> float
val video_frame_rate : stream:int -> mpeg -> float
val video_length : stream:int -> mpeg -> nativeint

val seek_video_position : stream:int -> pos:nativeint -> mpeg -> unit
val tell_video_position : stream:int -> mpeg -> nativeint

val seek_previous_frame : stream:int -> mpeg -> unit

val end_of_video : stream:int -> mpeg -> bool

val color_model : stream:int -> mpeg -> color_model
(* val set_rowspan : stream:int -> int -> mpeg -> unit *)

(* val read_frame : ??? *)
(* val read_yuvframe : ??? *)

val drop_frames : stream:int -> int -> mpeg -> unit

val read_video_chunk : stream:int ->
                       ?pos:int ->
                       ?length:int ->
                       string ->
                       mpeg ->
                       int

val create_frame_buffer : width:int -> height:int -> frame_buffer

val fb_width : frame_buffer -> int

val fb_height : frame_buffer -> int

val fb_array1 : frame_buffer -> (int,int8_unsigned_elt,c_layout) Array1.t
    (* Return the frame buffer as continous array. 3 bytes for each pixel
     * (= color model RGB888).
     *)

val fb_array2 : frame_buffer -> (int,int8_unsigned_elt,c_layout) Array2.t
    (* Return the frame buffer as matrix *)

val read_frame : ?in_x:int -> ?in_y:int -> ?in_w:int -> ?in_h:int ->
                 stream:int -> frame_buffer -> mpeg -> unit
    (* Decodes the current frame into the frame_buffer *)


(**********************************************************************)
(* Combined seeking                                                   *)
(**********************************************************************)

val seek_percentage : float -> mpeg -> unit
val tell_percentage : mpeg -> float

val get_time : mpeg -> float
  (* Returns the "Presentation Timestamp" of the last read packet (no matter
   * whether video or audio, or which stream)
   *)

