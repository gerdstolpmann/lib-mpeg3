TOP_DIR = .
include $(TOP_DIR)/Makefile.rules

all:
	$(MAKE) depend
	$(MAKE) -f Makefile.code all

opt:
	$(MAKE) depend
	$(MAKE) -f Makefile.code opt

depend:
	$(OCAMLDEP) *.ml *.mli >depend

clean:
	rm -f $(CLEAN_LIST)

CLEAN: clean

distclean: clean
	rm -f META

install:
	files=`$(COLLECT_FILES) *.mli *.cmi *.cma *.cmxa *.a dll* META` && \
	$(OCAMLFIND) install libmpeg3 $$files

uninstall:
	$(OCAMLFIND) remove libmpeg3
